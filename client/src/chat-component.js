// @flow

//import what we need
import * as React from 'react';
import { Component } from 'react-simplified';
import chatService from './chat-service';
import { Alert, Card, Form, Row, Column } from './widgets';

//declare class Chat
export class Chat extends Component {
  //declare needed variables

  subscription = null; //keep track of sub
  connected = false; //keep track of connected status
  messages: string[] = []; //all messages
  users: string[] = []; //all users connected
  user = ''; // this connected user
  message = ''; //this users message

  render() {
    return (
      <Card title={'Chat (' + (this.connected ? 'Connected' : 'Not connected') + ')'}>
        <Card title="Connected users">
          {this.users.map((
            user,
            i //display users from this.users
          ) => (
            <div key={i}>{user}</div>
          ))}
        </Card>
        <Card title="Messages">
          {this.messages.map((
            message,
            i //display messages from this.messages
          ) => (
            <div key={i}>{message}</div>
          ))}
        </Card>
        <Card title="New message">
          <Row>
            <Column width={2}>
              <Form.Input
                type="text"
                placeholder="User"
                disabled={this.subscription}
                value={this.user}
                onChange={(e) => (this.user = e.currentTarget.value)} //disabled when not subscribed, takes value of user, changes this.user onChange
                onKeyUp={(e: SyntheticKeyboardEvent<HTMLInputElement>) => {
                  if (e.key == 'Enter') {
                    // only when Enter is pressed and released, we go forward
                    if (!this.subscription) {
                      // Subscribe if not subbscribed
                      this.subscription = chatService.subscribe();

                      // sets connected to true, and adds user to this.user when subscription is open
                      this.subscription.onopen = () => {
                        this.connected = true;
                        chatService.send({ addUser: this.user });

                        // Remove user when web page is closed
                        window.addEventListener('beforeunload', () =>
                          chatService.send({ removeUser: this.user })
                        );
                      };

                      // When incoming message, add message to this.messages, as well - add user
                      this.subscription.onmessage = (message) => {
                        if (message.text) this.messages.push(message.text);
                        if (message.users) this.users = message.users;
                      };

                      // error message when subscription closes, sets this.connection to false
                      this.subscription.onclose = (code, reason) => {
                        this.connected = false;
                        Alert.danger(
                          'Connection closed with code ' + code + ' and reason: ' + reason
                        );
                      };

                      // error message, when subscription error, sets this.connected to false
                      this.subscription.onerror = (error) => {
                        this.connected = false;
                        Alert.danger('Connection error: ' + error.message);
                      };
                    }
                  }
                }}
              />
            </Column>
            <Column>
              <Form.Input
                type="text"
                placeholder="Message"
                value={this.message} //message input field
                onChange={(e) => (this.message = e.currentTarget.value)} //changes this.message onChange
                onKeyUp={(e: SyntheticKeyboardEvent<HTMLInputElement>) => {
                  //sends message on enter
                  if (e.key == 'Enter') {
                    if (this.connected) {
                      //only sends if this.connected is true
                      chatService.send({ text: this.user + ': ' + this.message });
                      this.message = ''; //resets message field
                    } else Alert.danger('Not connected to server'); //error msg if this.connected == false
                  }
                }}
              />
            </Column>
          </Row>
        </Card>
      </Card>
    );
  }

  // Unsubscribe from chatService when component is no longer in use
  beforeUnmount() {
    chatService.unsubscribe(this.subscription);
  }
}
